import {makeStyles, Box, Typography} from "@material-ui/core";

const useStyles = makeStyles({
    image: {
        background: `url(${'https://images.pexels.com/photos/115655/pexels-photo-115655.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'}) center/55% repeat-x #000`,
        width: '100%',
        height: '60vh',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        '& :first-child': {
            fontSize: 70,
            color: '#FFFFFF',
            lineHeight: 1
        },
        '& :last-child': {
            fontSize: 30,
            background: '#FFFFFF',
        }
    },
});
const Banner = () => {
    const classes = useStyles();
    return (
        <Box className={classes.image}>
            <Typography>Divya Jain</Typography>
            <Typography>My Personal Blog</Typography>
        </Box>
    );
}
export default Banner;
