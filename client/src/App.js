import './App.css';
import { Box } from "@material-ui/core";
import Header from './components/Header';
import Home from "./components/home/Home";
import React from 'react';
function App() {
  return (
      <>
          <Header />
          <Box style={{marginTop:64}}>
              <Home />
          </Box>
      </>
  );
}

export default App;
